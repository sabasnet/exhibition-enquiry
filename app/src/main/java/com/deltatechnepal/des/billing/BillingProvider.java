package com.deltatechnepal.des.billing;


/**
 * An interface that provides an access to Billing Library methods
 */
public interface BillingProvider {
    BillingManager getBillingManager();
}

