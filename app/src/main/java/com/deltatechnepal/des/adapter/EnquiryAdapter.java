package com.deltatechnepal.des.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.main.EnquiryDetailsActivity;
import com.deltatechnepal.des.model.EnquiryRecord;

import java.util.ArrayList;
import java.util.List;



public class EnquiryAdapter extends RecyclerView.Adapter<EnquiryAdapter.ViewHolder> implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<EnquiryRecord> enquiryList;
    private List<EnquiryRecord> enquiryListFiltered;
    private int lastPosition = -1;
    private View v;
    private EnquiryAdapter.ViewHolder mh;


    public EnquiryAdapter(Activity activity, List<EnquiryRecord> enquiryList) {
        this.activity = activity;
        this.enquiryList = enquiryList;
        this.enquiryListFiltered = enquiryList;
        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        private TextView tvCustomerName,tvCustomerNumber,tvProduct;
        private ViewGroup transitionsContainer;
        public LinearLayout viewBackground;
        public  ConstraintLayout viewForeground;




        public ViewHolder(View view) {

            super(view);
            viewBackground = view.findViewById(R.id.viewBackground);
            viewForeground = view.findViewById(R.id.viewForeground);
            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvCustomerName = view.findViewById(R.id.customerNameValue);
            this.tvCustomerNumber = view.findViewById(R.id.customerNumberValue);
            this.tvProduct = view.findViewById(R.id.productValue);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                  int position=getAdapterPosition();
                    Intent intent=new Intent(activity,EnquiryDetailsActivity.class);
                    intent.putExtra("name",enquiryListFiltered.get(position).getcustomerName());
                    intent.putExtra("address",enquiryListFiltered.get(position).getcustomerAddress());
                    intent.putExtra("email",enquiryListFiltered.get(position).getcustomerEmail());
                    intent.putExtra("number",enquiryListFiltered.get(position).getcustomerNumber());
                    intent.putExtra("notes",enquiryListFiltered.get(position).getcustomerNotes());
                    intent.putExtra("status",enquiryListFiltered.get(position).getcustomerStatus());
                    intent.putExtra("company",enquiryListFiltered.get(position).getcustomerCompany());
                    intent.putExtra("product",enquiryListFiltered.get(position).getProduct());
                    activity.startActivity(intent);



                }
            });


        }


    }

    @Override
    public EnquiryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_items_row,viewGroup, false);
        mh = new EnquiryAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(EnquiryAdapter.ViewHolder holder, int i) {
        EnquiryRecord record = enquiryListFiltered.get(i);
        holder.tvCustomerName.setText(record.getcustomerName());
        holder.tvCustomerNumber.setText(record.getcustomerNumber());
        holder.tvProduct.setText(record.getProduct());

        setAnimation(holder.itemView, i);


    }



    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }






    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    enquiryListFiltered = enquiryList;
                } else {
                    List<EnquiryRecord> filteredList = new ArrayList<>();
                    for (EnquiryRecord row : enquiryList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        String name=row.getcustomerName().toLowerCase();
                        String number=row.getcustomerNumber().toLowerCase();
                        String product=row.getProduct().toLowerCase();


                        if (name.contains(charString) || number.contains(charString) || product.contains(charString))
                                 {
                            filteredList.add(row);
                        }
                    }

                    enquiryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = enquiryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                enquiryListFiltered = (ArrayList<EnquiryRecord>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    @Override
    public int getItemCount() {
        return (enquiryListFiltered.size());
    }




}