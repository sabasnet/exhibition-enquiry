package com.deltatechnepal.des.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.des.R;


public class EnquiryDetailsActivity extends AppCompatActivity {
    TextView tvName,tvAddress,tvMobile,tvEmail,tvCompany,tvNotes,tvProduct,tvStatus;
    Context context;
    String email,name,number,address,status,notes,company,product;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);


       setUpDetails();







    }


    public void setUpDetails(){

        tvName=findViewById(R.id.customerNameValue);
        tvAddress=findViewById(R.id.customerAddressValue);
        tvCompany=findViewById(R.id.customerCompanyValue);
        tvEmail=findViewById(R.id.customerEmailValue);
        tvMobile=findViewById(R.id.customerNumberValue);
        tvNotes=findViewById(R.id.customerNotesValue);
        tvProduct=findViewById(R.id.productValue);
        tvStatus=findViewById(R.id.customerStatusValue);

        Intent intent=getIntent();
         name=intent.getStringExtra("name");
         number=intent.getStringExtra("number");
         address=intent.getStringExtra("address");
         company=intent.getStringExtra("company");
         email=intent.getStringExtra("email");
         notes=intent.getStringExtra("notes");
         status=intent.getStringExtra("status");
         product=intent.getStringExtra("product");

        tvName.setText(name);
        tvAddress.setText(address);
        tvEmail.setText(email);
        tvProduct.setText(product);
        tvStatus.setText(status);
        tvCompany.setText(company);
        tvMobile.setText(number);
        tvNotes.setText(notes);

     if(!TextUtils.isEmpty(status)) {
         switch (status) {

             case "hot":
                 tvStatus.setBackgroundColor(getResources().getColor(R.color.red));
                 break;
             case "warm":
                 tvStatus.setBackgroundColor(getResources().getColor(R.color.orange));
                 break;
             case "cold":
                 tvStatus.setBackgroundColor(getResources().getColor(R.color.blue));
                 break;
         }
     }
     else{

         tvStatus.setText("");
         tvStatus.setBackgroundColor(getResources().getColor(R.color.trans));
     }





    }




    public void call(View v){


        int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        if (callPermission == 0) {
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplication().startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getApplication().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            getApplication().startActivity(i);
        }
    }




    public void sendEmail(View v){

        if(!TextUtils.isEmpty(email)) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Hello");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }




    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }




}