package com.deltatechnepal.des.main;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.helper.ColorText;
import com.deltatechnepal.des.helper.CustomToast;
import com.deltatechnepal.des.helper.SharedPreManager;
import com.deltatechnepal.des.model.EnquiryRecord;
import com.deltatechnepal.des.model.Logs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;


public class EnquiryAddActivity extends AppCompatActivity {
    private static final String TAG = EnquiryAddActivity.class.getSimpleName();
    private Context mContext;
    EditText customerName,customerAddress,customerEmail,customerCompany,customerNotes,customerNumber,product;
    RadioGroup rgStatus;
    String comp,name,email,number,company,prod,address,notes;
    String status="";
    CustomToast cToast;
    boolean duplicateNumber=false;
     DatabaseReference mDatabaseRef,mDatabaseRef1;
     FirebaseDatabase mFirebaseDatabase;
    private ValueEventListener addListener;
    private long unixTime ;
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_enquiry);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);


        
        
        
        cToast=new CustomToast();

        //Creating objects of UI components

        customerName=findViewById(R.id.customer_name);
        customerAddress=findViewById(R.id.customer_address);
        customerEmail=findViewById(R.id.customer_email);
        customerCompany=findViewById(R.id.customer_company);
        customerNotes=findViewById(R.id.customer_notes);
        customerNumber=findViewById(R.id.customer_mobile);
        product=findViewById(R.id.product);

        ColorText.getInstance(mContext).multiColor(customerName,"Customer Name*");
        ColorText.getInstance(mContext).multiColor(customerNumber,"Customer Number*");





        rgStatus=findViewById(R.id.rgStatus);


        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){

                    case R.id.rbHot:
                        status="hot";
                        break;
                    case R.id.rbWarm:
                        status="warm";
                        break;
                    case R.id.rbCold:
                        status="cold";
                        break;




                }
            }
        });


        final Intent i =getIntent();
        boolean scanMode= i.getBooleanExtra("scan",false);
        if(scanMode){
            customerNumber.setText(i.getStringExtra("number"));
            customerEmail.setText(i.getStringExtra("email"));
            customerCompany.setText(i.getStringExtra("company").toUpperCase());
            customerAddress.setText(i.getStringExtra("address"));
            customerName.setText(i.getStringExtra("name"));


        }






    }




    public void submitEnquiry(View v){


         name=customerName.getText().toString();
         address=customerAddress.getText().toString();
         email=customerEmail.getText().toString();
         number=customerNumber.getText().toString();
         company=customerCompany.getText().toString();
         notes=customerNotes.getText().toString();
         prod=product.getText().toString();

        if(!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") &&
                !TextUtils.isEmpty(email))
        {
            customerEmail.setError("Invalid Email");
        }





        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(number)) {
            comp = SharedPreManager.getInstance(mContext).getCompanyName();
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            mDatabaseRef = mFirebaseDatabase.getReference
                    (comp + "/");
            final Query update = mDatabaseRef.orderByChild("customerNumber").equalTo(customerNumber.getText().toString());

            update.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange (DataSnapshot dataSnapshot){
                   if(dataSnapshot.exists()) {
                       cToast.showToast(EnquiryAddActivity.this, getResources().getDrawable(R.drawable.ic_info_white), "Mobile Number Already exists. Please Enter another number");

                   }
                   else{
                       addEnquiry();
                   }

                }

                @Override
                public void onCancelled (DatabaseError databaseError){
                    Log.e(TAG, "onCancelled", databaseError.toException());
                }
            });

        }
        else{

            cToast.showToast(EnquiryAddActivity.this,getResources().getDrawable(R.drawable.ic_info),"PLease Fill all Required Fields");
        }


    }



     public void addEnquiry(){
         unixTime=System.currentTimeMillis();
         EnquiryRecord record = new EnquiryRecord(unixTime / 1000, name, address, prod, email, company, number, status, notes);
         String rootKey = mDatabaseRef.push().getKey();
         mDatabaseRef.child(rootKey).setValue(record);
         addItemChangeListener();
         finish();
     }


    /**
     * User data change listener
     */
    private void addItemChangeListener() {
        // User data change listener
        addListener= mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EnquiryRecord record = dataSnapshot.getValue(EnquiryRecord.class);

                // Check for null
                if (record == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }
                String cMessage=SharedPreManager.getInstance(mContext).getcustomMessage();


                if(!TextUtils.isEmpty(email)) {
                    mDatabaseRef1 = mFirebaseDatabase.getReference
                            ("logs/");
                    String rootKey = mDatabaseRef.push().getKey();

                    Logs log = new Logs(name, email, comp, cMessage);
                    mDatabaseRef1.child(rootKey).setValue(log);
                }


                 cToast.showToast(EnquiryAddActivity.this,getResources().getDrawable(R.drawable.ic_done),"Saved Successfully");
                //Hide KeyBoard

                status="";
                customerName.getText().clear();
                customerAddress.getText().clear();
                customerCompany.getText().clear();
                customerEmail.getText().clear();
                customerNotes.getText().clear();
                customerNumber.getText().clear();
                rgStatus.clearCheck();

                mDatabaseRef.removeEventListener(addListener);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}
