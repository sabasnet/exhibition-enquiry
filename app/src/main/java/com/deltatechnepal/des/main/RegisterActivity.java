package com.deltatechnepal.des.main;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.helper.AESCrypt;
import com.deltatechnepal.des.helper.CustomToast;
import com.deltatechnepal.des.model.Company;
import com.deltatechnepal.des.model.EnquiryRecord;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class RegisterActivity extends AppCompatActivity {
    private Context mContext;
    private static final String TAG = RegisterActivity.class.getSimpleName();
    ImageView mobile;
    Button button;
    EditText etCName,etCEmail,etCPassword,etConformPass;
    TextInputLayout textInputLayout;
    ProgressDialog progressDialog;
    private DatabaseReference companies;
    private FirebaseDatabase mFirebaseInstance;
    private ValueEventListener addListener;
    CustomToast cToast;
    String companyName,companyEmail,companyPassword,conformPassword,companyMessage="Thank you for visting our stall!";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        cToast=new CustomToast();
        textInputLayout = findViewById(R.id.holder1);
        etCName = findViewById(R.id.company_name);
        etCEmail = findViewById(R.id.company_email);
        etCPassword = findViewById(R.id.company_password);
        etConformPass = findViewById(R.id.confirm_password);
        mobile=findViewById(R.id.mobile);
        button=findViewById(R.id.button);




    }


    public void SignIn(View v){
        final List<Company> comList=new ArrayList<>();
        companyName=etCName.getText().toString().trim();
        companyEmail=etCEmail.getText().toString().trim();
        companyPassword=etCPassword.getText().toString().trim();
        conformPassword=etConformPass.getText().toString().trim();

        if(TextUtils.isEmpty(companyName))
        {
            cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_info_white),"Please enter company name");

            return;
        }
        if(TextUtils.isEmpty(companyEmail))
        {
            cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_info_white),"Please enter company email");

            return;
        }

        if(TextUtils.isEmpty(companyPassword))
        {
            cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_info_white),"Password cannot be blank");

            return;
        }

        if(!companyPassword.matches(conformPassword))
        {
            cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_info_white),"Password does not match");

            return;

        }

        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Checking.Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);
        progressDialog.show();
        mFirebaseInstance = FirebaseDatabase.getInstance();

         // get reference to 'users' node
        companies = mFirebaseInstance.getReference().child("companies");
        Query query=companies.orderByChild("cName").equalTo(companyName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                if (dataSnapshot.exists()) {
                    // dataSnapshot is the "issue" node with all children with id 0

                        // do something with the individual "issues"
                    cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_info_white),"Company name already exists");





                }
                else
                {
                    String epassword="";
                    try{
                          epassword= AESCrypt.encrypt(companyPassword);
                    }
                    catch (Exception e){

                    }

                    Company com = new Company(companyMessage,companyName,epassword,companyEmail,0,false);
                    String rootKey = companies.push().getKey();
                    companies.child(rootKey).setValue(com);
                    addItemChangeListener();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });






    }



    private void addItemChangeListener() {
        // User data change listener
        addListener= companies.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EnquiryRecord record = dataSnapshot.getValue(EnquiryRecord.class);

                // Check for null
                if (record == null) {
                    Log.e(TAG, "Data is null!");
                    return;
                }
                cToast.showToast(RegisterActivity.this,getResources().getDrawable(R.drawable.ic_done),"Registered Successfully");
                //Hide KeyBoard


                companies.removeEventListener(addListener);
                finish();

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }

    public void openLoginPage(View v){

        Intent i =new Intent(mContext,LoginActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
