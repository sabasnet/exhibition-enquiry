package com.deltatechnepal.des.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.helper.ConnectionChecker;
import com.deltatechnepal.des.helper.CustomToast;
import com.deltatechnepal.des.helper.OkHttpNetworking;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.i18n.phonenumbers.PhoneNumberMatch;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OcrActivity extends AppCompatActivity {


    public static final String TAG = OcrActivity.class.getSimpleName();
    ImageView mCameraView;
    TextView mTextView;
    CameraSource mCameraSource;
    private static final int requestPermissionID = 101;
    String result;
    Bitmap bitmap;
    CustomToast ct;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        mContext=this;


        mCameraView = findViewById(R.id.surfaceView);
        mTextView = findViewById(R.id.text_view);
        ct=new CustomToast();
        startCameraSource();





    }



    private void startCameraSource() {

        //Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies not loaded yet");
            ct.showToast(OcrActivity.this,getResources().getDrawable(R.drawable.ic_info),"Downloading required libraries. Please Try aftersome time ");
        } else {


                        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(OcrActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    requestPermissionID);
                            return;
                        }


                        Intent intent=getIntent();
                        final String path=intent.getStringExtra("uri");
                   try {
                        bitmap = BitmapFactory.decodeFile(path);
                       ExifInterface exif = null;
                       try {
                           File pictureFile = new File(path);
                           exif = new ExifInterface(pictureFile.getAbsolutePath());
                       } catch (IOException e) {
                           e.printStackTrace();
                       }

                       int orientation = ExifInterface.ORIENTATION_NORMAL;

                       if (exif != null)
                           orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                       switch (orientation) {
                           case ExifInterface.ORIENTATION_ROTATE_90:
                               bitmap = rotateBitmap(bitmap, 90);
                               break;
                           case ExifInterface.ORIENTATION_ROTATE_180:
                               bitmap = rotateBitmap(bitmap, 180);
                               break;

                           case ExifInterface.ORIENTATION_ROTATE_270:
                               bitmap = rotateBitmap(bitmap, 270);
                               break;
                       }

                        mCameraView.setImageBitmap(bitmap);
                   }
                   catch (Exception e) {
                       Log.w(TAG, "Oops Error Occured");
                   }
                  Frame frame = new Frame.Builder().setBitmap(bitmap).build();

            SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
            String blocks = "";
            String lines = "";
            String words = "";
            String address="";
            for (int index = 0; index < textBlocks.size(); index++) {
                //extract scanned text blocks here
                TextBlock tBlock = textBlocks.valueAt(index);
                blocks = blocks + tBlock.getValue() + "\n" + "\n";

                for (Text line : tBlock.getComponents()) {
                    //extract scanned text lines here
                    lines = lines + line.getValue() + "\n";
                    if(line.getValue().contains(",") && TextUtils.isEmpty(address) && line.getValue().length()<35)
                        address=line.getValue();

                    for (Text element : line.getComponents()) {
                        //extract scanned integer here
                        if(element.getValue().matches("\\d+")){
                            words = words + element.getValue();
                        }
                    }
                }
            }
            if (textBlocks.size() == 0) {
                ct.showToast(OcrActivity.this,getResources().getDrawable(R.drawable.ic_info),"Error Occured");
                finish();
            } else {
                deleteCapturedImage(path);
                String phoneNumber="";
                final ArrayList<String> phoneNumbers = parseResults(blocks);

                if(phoneNumbers == null){
                    phoneNumber = "";
                }else{
                    if(!phoneNumbers.isEmpty())
                        try {
                        if(phoneNumbers.size()>1) {
                            phoneNumber=phoneNumbers.get(0).length()>phoneNumbers.get(1).length()?phoneNumbers.get(0):phoneNumbers.get(1);
                        }
                        else{
                            phoneNumber=phoneNumbers.get(0);
                        }
                            phoneNumber = phoneNumber.replaceAll("[^\\d.]", "");
                        }catch(IndexOutOfBoundsException e){
                            e.printStackTrace();
                            Toast.makeText(OcrActivity.this, "There is no text!", Toast.LENGTH_SHORT).show();
                        }
                }
                String email=parseEmail(blocks);
                String url =parseUrl(blocks);
                String company="";
                String name="";

                if(!TextUtils.isEmpty(url)) {
                    String[] parts = url.split("\\.");// String array, each element is text between dots
                     if(parts.length>1)
                     company = parts[1];
                }

                if(ConnectionChecker.getInstance(mContext).Check()) {

                   try {
                        name = parseName(blocks);
                   }
                   catch (Exception e){
                       Log.e("Error",e.toString());
                   }

                }

                Intent i =new Intent(OcrActivity.this,EnquiryAddActivity.class);
                i.putExtra("scan",true);
                i.putExtra("number",phoneNumber);
                i.putExtra("email",email);
                i.putExtra("website",url);
                i.putExtra("company",company);
                i.putExtra("address",address);
                i.putExtra("name",name);
                startActivity(i);
                finish();

               /* mTextView.setText("Number: "+phoneNumber+"\nEmail :"+email+"\nWebsite :"+url+"\nCompany :"+company);*/


            }

        }


    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    private void deleteCapturedImage(String mCurrentPhotoPath) {
        File fileToBeDeleted = new File(mCurrentPhotoPath);
        if(fileToBeDeleted.exists()){
            if(fileToBeDeleted.delete()){
                Log.w(TAG, "File Deleted: " + mCurrentPhotoPath);
            } else {
                Log.w(TAG, "File Not Deleted " + mCurrentPhotoPath);
            }
        }
    }



    /**
     * Parses phoneNumbers from a string using Google's libphonenumber library
     *
     * @param bCardText, The text obtained from the vision API processing
     * @return ArrayList of parsed phone numbers from the vision API processed text string
     */
    private ArrayList<String> parseResults(String bCardText) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Iterable<PhoneNumberMatch> numberMatches = phoneNumberUtil.findNumbers(bCardText, Locale.US.getCountry());
        ArrayList<String> data = new ArrayList<>();
        for(PhoneNumberMatch number : numberMatches){
            String s = number.rawString();
            data.add(s);
        }
        return data;
    }


    /**
     * Parses email from the string returned from Google Vision APi
     * @param results, String returned from Google Vision API
     * @return String that is the parsed email. Uses REGEX
     */
    private String parseEmail(String results) {
        Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(results);
        String parsedEmail = "";
        while (m.find()) {
            parsedEmail = m.group();
        }
        return parsedEmail;
    }

    private String parseUrl(String results) {
        Matcher m = Pattern.compile("(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))").matcher(results);
        String parsedUrl = "";
        while (m.find()) {
            parsedUrl = m.group();
        }
        return parsedUrl;
    }


    /**
     * Parses name from the string returned from Google Vision APi
     * @param results, String returned from Google Vision API
     * @return String that is the parsed email. Picks first two strings from the param
     */
    private String parseName(String results) throws ExecutionException, InterruptedException {
        OkHttpNetworking okHttpNetworking = new OkHttpNetworking(results);
        return okHttpNetworking.execute().get();
    }


}