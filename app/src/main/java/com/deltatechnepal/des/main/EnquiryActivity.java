package com.deltatechnepal.des.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.adapter.EnquiryAdapter;
import com.deltatechnepal.des.billing.BillingManager;
import com.deltatechnepal.des.billing.BillingProvider;
import com.deltatechnepal.des.helper.CustomToast;
import com.deltatechnepal.des.helper.MConstant;
import com.deltatechnepal.des.helper.SharedPreManager;
import com.deltatechnepal.des.model.Company;
import com.deltatechnepal.des.model.EnquiryRecord;
import com.deltatechnepal.des.skulist.AcquireFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;


public class EnquiryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,BillingProvider {
    private Context mContext;
    private static final int IMAGE_PERMISSION = 4 ;
    private static final int IMAGE_CAPTURE_REQUEST = 1001;
    SearchView searchView;
    Uri photoURI;

    private String TAG = EnquiryActivity.class.getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvRecords;
    private LinearLayoutManager linearLayoutManager;
    private EnquiryAdapter adapter;
    private List<EnquiryRecord> enquiryList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabase,mDatabaseRef,companies;
    private FirebaseDatabase mFirebaseInstance,nFirebaseInstance;
    private ValueEventListener mListener;
    String companyName;
    private int sort;
    FloatingActionButton fabAdd,fabScan;
    AlertDialog b;
    int clickedItem;
    DrawerLayout mDrawerLayout;
    EditText customerName,customerAddress,customerEmail,customerCompany,customerNotes,customerNumber,product;
    RadioGroup rgStatus;
    String mCurrentPhotoPath;
    private ValueEventListener addListener,messListener;
    String ustatus,uname,unumber,ucompany,uaddress,uemail,unotes,uproduct;
    Bitmap bitmap;

    private BillingManager mBillingManager;
    private AcquireFragment mAcquireFragment;
    private static final String DIALOG_TAG = "dialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;

        mFirebaseInstance = FirebaseDatabase.getInstance();
        companies = mFirebaseInstance.getReference().child("companies");

        // Try to restore dialog fragment if we were showing it prior to screen rotation
        if (savedInstanceState != null) {
            mAcquireFragment = (AcquireFragment) getSupportFragmentManager()
                    .findFragmentByTag(DIALOG_TAG);
        }

        // Create and initialize BillingManager which talks to BillingLibrary
        mBillingManager = new BillingManager(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_drawer);


        fabAdd = findViewById(R.id.fabAdd);
        fabScan = findViewById(R.id.fabScan);
        //setting Swipe to refresh and Recyclerview

        rvRecords = findViewById(R.id.rvRecords);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvRecords.setLayoutManager(linearLayoutManager);
        rvRecords.setHasFixedSize(true);
        rvRecords.setNestedScrollingEnabled(true);
        final Animation animationLeft = AnimationUtils.loadAnimation(mContext,
                R.anim.s_left);
        animationLeft.setDuration(500);
        final Animation animationRight = AnimationUtils.loadAnimation(mContext,
                R.anim.s_right);
        animationRight.setDuration(500);


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvRecords);

        rvRecords.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    //this is the top of the RecyclerView
                    fabAdd.setVisibility(View.VISIBLE);
                    fabAdd.startAnimation(animationLeft);
                    fabScan.setVisibility(View.VISIBLE);
                    fabScan.startAnimation(animationLeft);
                } else {
                    if (fabAdd.getVisibility() == View.VISIBLE) {
                        fabAdd.setVisibility(View.GONE);
                        fabAdd.startAnimation(animationRight);
                        fabScan.setVisibility(View.GONE);
                        fabScan.startAnimation(animationRight);
                    }
                }


            }
        });


        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setRefreshing(true);

        companyName= SharedPreManager.getInstance(mContext).getCompanyName();
        fetchItems();

        //region Permissions
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE,Manifest.permission.CAMERA}, 101);
        //endregion



        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {


            }
        });


        addMessageChangeListener();


        mDrawerLayout = findViewById(R.id.drawer_layout);



        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        mDrawerLayout.closeDrawers();

                        /*menuItem.setChecked(true);*/
                        // close drawer when item is tapped


                        switch (menuItem.getItemId()){
                            case R.id.nav_logout: {
                                new AlertDialog.Builder(mContext)
                                        .setTitle(getString(R.string.app_name))
                                        .setMessage("Want To Logout?")
                                        .setIcon(R.drawable.ic_info)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                SharedPreManager.getInstance(mContext).logout();
                                                ActivityOptions options =
                                                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
                                                Intent intent = new Intent(mContext, LoginActivity.class);
                                                startActivity(intent, options.toBundle());
                                                finish();
                                            }})
                                        .setNegativeButton("No", null).show();


                                break;
                            }

                            case R.id.nav_create_item: {
                                clickedItem=R.id.nav_create_item;
                                break;
                            }

                            case R.id.nav_upgrade: {
                                //showPurchaseDialog(drawerView);// Using AlertDialog
                                showPurchaseDialog();

                                break;
                            }

                            case R.id.nav_aboutus: {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MConstant.DEVELOPER_URL)));
                                break;
                            }



                        }




                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        View navHeader = navigationView.getHeaderView(0);
        TextView tvCompany=navHeader.findViewById(R.id.companyName);
        tvCompany.setText(companyName);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Called when a drawer's position changes.
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // Called when a drawer has settled in a completely closed state.
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

                switch (clickedItem){

                    case R.id.nav_create_item: {
                        Intent intent = new Intent(mContext, EnquiryAddActivity.class);
                        startActivity(intent, options.toBundle());
                        break;
                    }






                }

                clickedItem=0;


            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
            }
        });











    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((EnquiryAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((EnquiryAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((EnquiryAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView

            final int position = viewHolder.getAdapterPosition();

            if(direction==ItemTouchHelper.RIGHT) {
                //Remove swiped item from list and notify the RecyclerView
                int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
                if (callPermission == 0) {
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String number = enquiryList.get(position).getcustomerNumber();

                    intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplication().startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
                    final Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + getApplication().getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    getApplication().startActivity(i);
                }
            }
            else{

                editItem(position);


            }
        }
    };


    public void editItem(final int position){


            ustatus="";
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            final LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_item_edit, null);
            dialogBuilder.setView(dialogView);


        customerName=dialogView.findViewById(R.id.customer_name);
        customerAddress=dialogView.findViewById(R.id.customer_address);
        customerEmail=dialogView.findViewById(R.id.customer_email);
        customerCompany=dialogView.findViewById(R.id.customer_company);
        customerNotes=dialogView.findViewById(R.id.customer_notes);
        customerNumber=dialogView.findViewById(R.id.customer_mobile);
        product=dialogView.findViewById(R.id.product);


        rgStatus=dialogView.findViewById(R.id.rgStatus);


           final EnquiryRecord item=enquiryList.get(position);

            customerName.setText(item.getcustomerName());
            customerAddress.setText(item.getcustomerAddress());
            customerCompany.setText(item.getcustomerCompany());
            customerEmail.setText(item.getcustomerEmail());
            customerNotes.setText(item.getcustomerNotes());
            customerNumber.setText(item.getcustomerNumber());
            product.setText(item.getProduct());

            if(!TextUtils.isEmpty(item.getcustomerStatus())) {
                switch (item.getcustomerStatus()) {
                    case "hot":
                        rgStatus.check(R.id.rbHot);
                        break;
                    case "warm":
                        rgStatus.check(R.id.rbWarm);
                        break;
                    case "cold":
                        rgStatus.check(R.id.rbCold);
                        break;
                }
            }

        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){

                    case R.id.rbHot:
                        ustatus="hot";
                        break;
                    case R.id.rbWarm:
                        ustatus="warm";
                        break;
                    case R.id.rbCold:
                        ustatus="cold";
                        break;




                }
            }
        });


            dialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //do something with edt.getText().toString();


                    uname=customerName.getText().toString();
                    uaddress=customerAddress.getText().toString();
                    ucompany=customerCompany.getText().toString();
                    uemail=customerEmail.getText().toString();
                    unotes=customerNotes.getText().toString();
                    unumber=customerNumber.getText().toString();
                    uproduct=product.getText().toString();


                    final Query update = mFirebaseDatabase.orderByChild("customerNumber").equalTo(item.getcustomerNumber());

                    update.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange (DataSnapshot dataSnapshot){
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                postSnapshot.getRef().removeValue();

                                String rootKey = mFirebaseDatabase.push().getKey();
                                mDatabaseRef = mFirebaseDatabase.child(rootKey);
                                EnquiryRecord record = new EnquiryRecord(item.getEnquiryDate(),uname, uaddress, uproduct, uemail, ucompany, unumber, ustatus, unotes);
                                mDatabaseRef.setValue(record);
                                addItemChangeListener();
                                adapter.notifyItemChanged(position);
                            }
                        }

                        @Override
                        public void onCancelled (DatabaseError databaseError){
                            Log.e(TAG, "onCancelled", databaseError.toException());
                        }
                    });










                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  adapter.notifyItemChanged(position);
                }
            });


            b = dialogBuilder.create();
            b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            b.show();
            b.setTitle("Edit");
            dialogBuilder.setMessage(null);






        }



    private void addMessageChangeListener() {
        // User data change listener
        Query query= companies.orderByChild("cName").equalTo(companyName);
        messListener= query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Company com = postSnapshot.getValue(Company.class);

                    // Check for null
                    if (com == null) {
                        Log.e(TAG, "User data is null!");
                        return;
                    }

                    SharedPreManager.getInstance(mContext).customMessage(com.getcMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }




    /**
     * User data change listener
     */
    private void addItemChangeListener() {
        // User data change listener
        addListener= mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EnquiryRecord item = dataSnapshot.getValue(EnquiryRecord.class);

                // Check for null
                if (item == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                CustomToast cToast= new CustomToast();
                cToast.showToast(EnquiryActivity.this,
                        getResources().getDrawable(R.drawable.ic_done),
                        "Successfully Updated ");
                mDatabaseRef.removeEventListener(addListener);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }















    public void deleteItem(final int position) {

    /*    Item item=itemList.get(position);

    final Query delete = mFirebaseDatabase.orderByChild("itemCode").equalTo(item.itemCode);

        delete.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange (DataSnapshot dataSnapshot){
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                postSnapshot.getRef().removeValue();
                itemList.remove(position);
                adapter.notifyItemRemoved(position);
            }
        }

        @Override
        public void onCancelled (DatabaseError databaseError){
            Log.e(TAG, "onCancelled", databaseError.toException());
        }
    });*/
}


    /**
     * This method is called when swipe refresh is pulled down
     */
    public void onRefresh() {
        if(searchView.isIconified()) {
            swipeRefreshLayout.setRefreshing(true);
            fetchItems();
            swipeRefreshLayout.setRefreshing(false);
        }
        swipeRefreshLayout.setRefreshing(false);
    }



    public void fetchItems()
    {


        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference
                (companyName+"/");


       mListener= mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                enquiryList.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {

                    if (postSnapshot != null) {
                        EnquiryRecord enquiryRecord = postSnapshot.getValue(EnquiryRecord.class);
                        enquiryList.add(enquiryRecord);
                    }


                    // here you can access to name property like university.name

                }

                sortList();
                adapter = new EnquiryAdapter(EnquiryActivity.this, enquiryList);
                rvRecords.setAdapter(adapter);




                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });



    }


    public void AddItem(View v){

        Intent intent = new Intent(mContext, EnquiryAddActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
        startActivity(intent, options.toBundle());



    }


    public void openScanner(View v){

        startCameraActivityIntent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMAGE_CAPTURE_REQUEST && resultCode == RESULT_OK){
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

            Intent intent = new Intent(mContext, OcrActivity.class);
            intent.putExtra("uri",mCurrentPhotoPath);
            startActivity(intent, options.toBundle());

        }
    }

    /**
     * Starts the camera and requests permission to use the camera if permission doesn't exist
     *
     */
    public void startCameraActivityIntent(){
        //Required camera permission
        String[] permissions = {"android.permission.CAMERA"};
        //Intent to startCamera
        Intent startCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager
                .PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, permissions, IMAGE_PERMISSION);
        }
        else {
            if (startCameraIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = createImageFile();
                if(photoFile != null) {
                    try {
                        photoURI = FileProvider.getUriForFile(this, "com.deltatechnepal.android.FileProvider", photoFile);
                    }
                    catch (Exception e){

                    }
                    //For non bitmap full sized images use EXTRA_OUTPUT during Intent
                    startCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(startCameraIntent, IMAGE_CAPTURE_REQUEST);
                }
            }
        }
    }


    private File createImageFile(){
        //Create image filename
        String imageFileName = "JPEG_00";

        //Access storage directory for photos and create temporary image file
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(imageFileName,".jpg",storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Store file path for usage with intents
        assert image != null;
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }




    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);

                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });





        // Get the search close button image view
        ImageView closeButton =searchView.findViewById(R.id.search_close_btn);

        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Find EditText view
                EditText et = searchView.findViewById(R.id.search_src_text);

                //Clear the text from EditText view
                et.setText("");

            }
        });

        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                 mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
            case R.id.action_sort:
            {

                showSortDialog();
                return true;


            }
            case R.id.action_share:
            {

                ExportDatabaseCSVTask task=new ExportDatabaseCSVTask();
                task.execute();
                return true;

            }
            case R.id.action_message:
            {
                openMessageDialog();
                return true;
            }


            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public BillingManager getBillingManager() {
        return mBillingManager;
    }

    private class ExportDatabaseCSVTask extends AsyncTask<String ,String, String> {
        private final ProgressDialog dialog = new ProgressDialog(EnquiryActivity.this);

        protected void onPreExecute() {
            this.dialog.setMessage("Exporting database...");
            this.dialog.show();
        }


    protected String doInBackground(final String... args){
        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, "EnquiryListExcel.csv");
        try {

            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

            //Headers
            String arrStr1[] ={"Cusomer Name", "Customer Address", "Customer Mobile", "Customer Email","Customer Company",
            "Product/Service","Customer Status","Customer Notes"};
            csvWrite.writeNext(arrStr1);

            for(EnquiryRecord enquiry:enquiryList) {

                String arrStr[] = {enquiry.getcustomerName(),enquiry.getcustomerAddress(), enquiry.getcustomerNumber()
                        , enquiry.getcustomerEmail(),enquiry.getcustomerCompany(),enquiry.getProduct(),
                        enquiry.getcustomerStatus(),
                enquiry.getcustomerNotes()};
                csvWrite.writeNext(arrStr);


            }

            csvWrite.close();
            return "";
        }
        catch (IOException e){
            Log.e("EnquiryActivity", e.getMessage(), e);
            return "";
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPostExecute(final String success) {

        if (this.dialog.isShowing()){
            this.dialog.dismiss();
        }
        if (success.isEmpty()){
            Toast.makeText(EnquiryActivity.this, "Export successful!", Toast.LENGTH_SHORT).show();
            onShareClick();

        }
        else {
            Toast.makeText(EnquiryActivity.this, "Export failed!", Toast.LENGTH_SHORT).show();
        }
    }
}


    public void onShareClick() {
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        Resources resources = getResources();
        String filename="EnquiryListExcel.csv";
        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);
        Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Excel file is in the attachements");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Enquiry List");
        emailIntent .putExtra(Intent.EXTRA_STREAM, path);
        emailIntent.setType("message/rfc822");

        PackageManager pm = getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");


        Intent openInChooser = Intent.createChooser(emailIntent, "Send Via");

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if(packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if(packageName.contains("whatsapp")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("application/csv");
                intent.putExtra(Intent.EXTRA_TEXT, "Excel file is in the attachements");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Enquiry List");
                intent .putExtra(Intent.EXTRA_STREAM, path);
                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }



    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


    public void showSortDialog(){


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_sort, null);
        dialogBuilder.setView(dialogView);





        final RadioGroup radioGroup=dialogView.findViewById(R.id.sort_radio_group);
        final RadioButton rbName=radioGroup.findViewById(R.id.name);
        final RadioButton rbProduct=radioGroup.findViewById(R.id.product);
        final RadioButton rbDate=radioGroup.findViewById(R.id.date);
        final CheckBox cbReverse=dialogView.findViewById(R.id.cbReverse);

        int sortValue=SharedPreManager.getInstance(mContext).getSortValue();
        switch (sortValue)
        {
            case 1:
                rbName.setChecked(true);
                break;
            case 2:
                rbProduct.setChecked(true);
                break;
            case 3:
                rbDate.setChecked(true);
                break;


        }
        if(SharedPreManager.getInstance(mContext).isSortReversed())
            cbReverse.setChecked(true);







        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                int position=radioGroup.getCheckedRadioButtonId();

                if(position == rbName.getId()) {
                   sort=1;

                } else if(position == rbProduct.getId()) {
                    sort=2;
                }
                else{
                    sort=3;
                }


                SharedPreManager.getInstance(mContext).sortItemValue(sort);


                if(cbReverse.isChecked())
                    SharedPreManager.getInstance(mContext).sortOrder(true);
                else
                    SharedPreManager.getInstance(mContext).sortOrder(false);


                sortList();
                adapter.notifyDataSetChanged();


                }


        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog b = dialogBuilder.create();
        b.setTitle("Sort by:");
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();






    }

    public void openMessageDialog(){


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_message, null);
        dialogBuilder.setView(dialogView);

        final EditText etMessage=dialogView.findViewById(R.id.custom_message);

        String msg = SharedPreManager.getInstance(mContext).getcustomMessage();

        etMessage.setText(msg);
        etMessage.setSelection(msg.length());

        etMessage.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etMessage.getText().toString().length() == 1 && etMessage.getTag().toString().equals("true")) {
                    etMessage.setTag("false");
                    etMessage.setText(etMessage.getText().toString().toUpperCase());
                    etMessage.setSelection(etMessage.getText().toString().length());
                }
                if(etMessage.getText().toString().length() == 0) {
                    etMessage.setTag("true");
                }

               /* if (null != etMessage.getLayout() && etMessage.getLayout().getLineCount() > 25) {
                    etMessage.getText().delete(etMessage.getText().length() - 1, etMessage.getText().length());
                }*/
            }
        });

        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                Query query=companies.orderByChild("cName").equalTo(companyName);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                // do something with the individual "issues"


                                issue.getRef().child("cMessage").setValue(etMessage.getText().toString());
                                SharedPreManager.getInstance(mContext).customMessage(etMessage.getText().toString());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }


        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog b = dialogBuilder.create();
        b.setTitle("Message");
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();

    }


    public void sortList(){

       int sortValue=SharedPreManager.getInstance(mContext).getSortValue();

        switch (sortValue){
            case 1:
                Collections.sort(enquiryList, new Comparator<EnquiryRecord>() {
                    @Override
                    public int compare(EnquiryRecord lhs, EnquiryRecord rhs) {
                        return lhs.getcustomerName().compareToIgnoreCase(rhs.getcustomerName());
                    }
                });
                break;
            case 2:
                Collections.sort(enquiryList, new Comparator<EnquiryRecord>() {
                    @Override
                    public int compare(EnquiryRecord lhs, EnquiryRecord rhs) {
                        return lhs.getProduct().compareToIgnoreCase(rhs.getProduct());
                    }
                });
                break;
            case 3:
                Collections.sort(enquiryList, new Comparator<EnquiryRecord>() {
                    @Override
                    public int compare(EnquiryRecord lhs, EnquiryRecord rhs) {
                        return (int) (lhs.getEnquiryDate()-rhs.getEnquiryDate());
                    }
                });
                break;






        }

        if(SharedPreManager.getInstance(mContext).isSortReversed()){
            Collections.reverse(enquiryList);
        }





    }




    @Override
    public void onDestroy(){

        super.onDestroy();
        mFirebaseDatabase.removeEventListener(mListener);
        companies.removeEventListener(messListener);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.app_name))
                    .setMessage("Want To Exit?")
                    .setIcon(R.drawable.ic_info)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            finish();
                        }})
                    .setNegativeButton("No", null).show();
        }
    }


    public void showPurchaseDialog() {

        if (mAcquireFragment == null) {
            mAcquireFragment = new AcquireFragment();
        }

        if (!isAcquireFragmentShown()) {
            mAcquireFragment.show(getSupportFragmentManager(), DIALOG_TAG);
        }
    }

    public boolean isAcquireFragmentShown() {
        return mAcquireFragment != null && mAcquireFragment.isVisible();
    }


    public void sendTestEmail(){
        Toast.makeText(mContext, "sendTestEmail() Clicked", Toast.LENGTH_SHORT).show();

    }


}
