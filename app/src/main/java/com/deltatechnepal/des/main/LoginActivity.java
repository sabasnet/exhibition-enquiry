package com.deltatechnepal.des.main;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.des.R;
import com.deltatechnepal.des.helper.AESCrypt;
import com.deltatechnepal.des.helper.CustomToast;
import com.deltatechnepal.des.helper.SharedPreManager;
import com.deltatechnepal.des.model.Company;
import com.deltatechnepal.des.model.EnquiryRecord;
import com.google.android.gms.vision.text.Text;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class LoginActivity extends AppCompatActivity {
    private Context mContext;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    ImageView mobile;
    int submitMode=1,recoveryCode;
    Button btnForgot,btnSubmit;
    EditText etCName,etCPassword,etCNewPassword;
    TextInputLayout holder1,holder2,holder3;
    TextInputLayout textInputLayout;
    FloatingActionButton fabbutton;
    ProgressDialog progressDialog;
    String mVerificationId;
    Boolean mVerified = false;
    private DatabaseReference companies;
    private FirebaseDatabase mFirebaseInstance;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    CustomToast cToast;
    private ValueEventListener addListener;
    String companyName,companyPassword,companyEmail;
    TextView tvRetry;
    DataSnapshot reference;
    PhoneAuthProvider.ForceResendingToken retryToken;
    ChildEventListener companyNameListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        cToast=new CustomToast();
        textInputLayout = findViewById(R.id.holder1);
        etCName = findViewById(R.id.company_name);
        etCPassword = findViewById(R.id.company_password);
        etCNewPassword = findViewById(R.id.new_password_confirm);
        mobile=findViewById(R.id.mobile);
        btnForgot=findViewById(R.id.btnForgot);
        btnSubmit=findViewById(R.id.btnSubmit);

        holder1=findViewById(R.id.holder1);
        holder2=findViewById(R.id.holder2);
        holder3=findViewById(R.id.holder3);


        //Check if ALready Logged In
        if(SharedPreManager.getInstance(mContext).isLoggedIn())
        {
            startActivity(new Intent(LoginActivity.this, EnquiryActivity.class));
            finish();
        }

        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'users' node
        companies = mFirebaseInstance.getReference().child("companies");

        addItemChangeListener();


    }


    public void SignIn(View v){
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Checking.Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);
        progressDialog.show();


        if(submitMode==1){
         submitLogin();
     }
     else if(submitMode==2){

         companyEmail=etCName.getText().toString();

         Query query=companies.orderByChild("cEmail").equalTo(companyEmail);
         query.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot dataSnapshot) {
                 progressDialog.dismiss();
                 if (dataSnapshot.exists()) {
                     // dataSnapshot is the "issue" node with all children with id 0
                     for (DataSnapshot issue : dataSnapshot.getChildren()) {
                         // do something with the individual "issues"
                          reference=issue;
                         final int min = 1000;
                         final int max = 9999;
                         final int random = new Random().nextInt((max - min) + 1) + min;
                         Company ocom=issue.getValue(Company.class);
                         Company company=new Company(ocom.getcMessage(),ocom.getcName(),ocom.getcPassword(),ocom.getcEmail(),random,true);
                         issue.getRef().setValue(company);
                         holder1.setHint("Enter Recovery Code");
                         btnSubmit.setText("Verify");
                         submitMode=3;
                         recoveryCode=random;
                         cToast.showToast(LoginActivity.this,
                                 getResources().getDrawable(R.drawable.ic_done),
                                 "Recover Code Sent to "+companyEmail);
                         issue.getRef().child("cForgot").setValue(false);
                         etCName.getText().clear();




                     }
                 }
             }

             @Override
             public void onCancelled(DatabaseError databaseError) {

             }
         });




     }
     else if(submitMode==3) {

            if (etCName.getText().toString().matches(String.valueOf(recoveryCode))) {
                holder1.setVisibility(View.GONE);
                holder2.setVisibility(View.VISIBLE);
                holder2.setHint("New Password");
                holder3.setVisibility(View.VISIBLE);
                submitMode = 4;

            }
            else {
                cToast.showToast(LoginActivity.this,
                        getResources().getDrawable(R.drawable.ic_done),
                        "Incorrect Code");

            }
        }

         else{

             if(etCPassword.getText().toString().matches(etCNewPassword.getText().toString()) && etCPassword.length()>0){

                 String newPassword ="";

                 try {
                     newPassword=AESCrypt.encrypt(etCPassword.getText().toString());
                 }
                 catch (Exception e){

                 }
                 reference.getRef().child("cPassword").setValue(newPassword);

                 holder1.setVisibility(View.VISIBLE);
                 holder1.setHint("Company Name");
                 holder2.setHint("Company Password");
                 holder3.setVisibility(View.GONE);
                 etCName.getText().clear();
                 etCPassword.getText().clear();
                 btnSubmit.setText("Submit");
                 submitMode=1;
                 cToast.showToast(LoginActivity.this,
                         getResources().getDrawable(R.drawable.ic_done),
                         "Password Changed Successfully");




             }
             else{

                 cToast.showToast(LoginActivity.this,
                         getResources().getDrawable(R.drawable.ic_done),
                         "Passwords not matching");
             }









         }






        progressDialog.dismiss();

    }



    public void submitLogin(){

        final List<Company> comList=new ArrayList<>();
        companyName=etCName.getText().toString().trim();
        companyPassword=etCPassword.getText().toString().trim();

        if(TextUtils.isEmpty(companyName)){
            etCName.setError(getResources().getString(R.string.error_required_field));
            etCName.requestFocus();
            return;
        } else if(TextUtils.isEmpty(companyPassword)){
            etCPassword.setError(getResources().getString(R.string.error_required_field));
            etCPassword.requestFocus();
            return;
        }




        Query query=companies.orderByChild("cName").equalTo(companyName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    // dataSnapshot is the "issue" node with all children with id 0
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"



                        Company company= issue.getValue(Company.class);

                        try {
                            String dPassword= AESCrypt.decrypt(company.getcPassword());
                            if(dPassword.matches(companyPassword)){

                                cToast.showToast(LoginActivity.this,
                                        getResources().getDrawable(R.drawable.ic_done),
                                        "Login Successful");
                                SharedPreManager.getInstance(mContext).userLogin(company);
                                Intent intent=new Intent(mContext,EnquiryActivity.class);
                                startActivity(intent);
                                finish();



                            }
                            else {
                                cToast.showToast(LoginActivity.this,
                                        getResources().getDrawable(R.drawable.ic_done),
                                        "Credentials do not match");
                            }
                        }
                        catch (Exception e){

                        }

                    }
                }
                else
                {
                    cToast.showToast(LoginActivity.this,
                            getResources().getDrawable(R.drawable.ic_done),
                            "Company Not Present");

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

     public void forgotPassword(View v){
        holder2.setVisibility(View.GONE);
        holder3.setVisibility(View.GONE);
        btnForgot.setVisibility(View.GONE);
        holder1.setHint("Company Email");
        btnSubmit.setText("Recover");
        submitMode=2;




     }


    private void addItemChangeListener() {
        // User data change listener
        addListener= companies.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EnquiryRecord record = dataSnapshot.getValue(EnquiryRecord.class);

                // Check for null
                if (record == null) {
                    Log.e("LoginActivity", "Data is null!");
                    return;
                }





            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e("LoginActivity", "Failed to read user", error.toException());
            }
        });


    }

    public void openRegisterPage(View v){

        Intent i =new Intent(mContext,RegisterActivity.class);
        startActivity(i);
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }
    @Override
    public void onBackPressed() {
       if(submitMode!=1){
           holder1.setVisibility(View.VISIBLE);
           holder1.setHint("Company Name");
           holder2.setVisibility(View.VISIBLE);
           holder2.setHint("Company Password");
           holder3.setVisibility(View.GONE);
           btnForgot.setVisibility(View.VISIBLE);
           etCName.getText().clear();
           etCPassword.getText().clear();
           btnSubmit.setText("Submit");
           submitMode=1;
       }
    }

    @Override
    public void finish() {
        super.finish();
        if(companies != null) {

            companies.removeEventListener(addListener);
        }
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
