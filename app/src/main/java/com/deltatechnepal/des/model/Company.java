package com.deltatechnepal.des.model;


public class Company {


    private String cName;
    private String cPassword;
    private String cMessage;
    private String cEmail;
    private long cRecovery;
    private boolean cForgot;




    public Company(){

    }


    public Company(String cMessage,String cName, String cPassword,String cEmail,long cRecovery,boolean cForgot) {
        this.cMessage = cMessage;
        this.cName = cName;
        this.cPassword = cPassword;
        this.cEmail=cEmail;
        this.cRecovery=cRecovery;
        this.cForgot=cForgot;

    }

    public String getcName() {
        return cName;
    }

    public String getcPassword() {
        return cPassword;
    }

    public String getcMessage() {
        return cMessage;
    }

    public String getcEmail() {
        return cEmail;
    }

    public long getcRecovery() {
        return cRecovery;
    }

    public boolean iscForgot() {
        return cForgot;
    }
}
