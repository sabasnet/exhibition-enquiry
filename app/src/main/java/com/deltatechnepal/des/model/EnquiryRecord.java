package com.deltatechnepal.des.model;


public class EnquiryRecord {

    private long enquiryDate;
    private String customerName;
    private String customerAddress;
    private String product;
    private String customerEmail;
    private String customerCompany;
    private String customerNumber;
    private String customerStatus;
    private String customerNotes;


    public EnquiryRecord(){

    }


    public EnquiryRecord(long enquiryDate,String customerName, String customerAddress, String product,String customerEmail,String customerCompany
    ,String customerNumber,String customerStatus,String customerNotes) {
        this.enquiryDate=enquiryDate;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.product=product;
        this.customerEmail=customerEmail;
        this.customerCompany=customerCompany;
        this.customerNumber=customerNumber;
        this.customerStatus=customerStatus;
        this.customerNotes=customerNotes;

    }

    public String getcustomerName() {
        return customerName;
    }

    public String getcustomerAddress() {
        return customerAddress;
    }

    public String getProduct() {
        return product;
    }

    public String getcustomerCompany() {
        return customerCompany;
    }

    public String getcustomerEmail() {
        return customerEmail;
    }

    public String getcustomerNotes() {
        return customerNotes;
    }

    public String getcustomerNumber() {
        return customerNumber;
    }

    public String getcustomerStatus() {
        return customerStatus;
    }

    public long getEnquiryDate() {
        return enquiryDate;
    }
}
