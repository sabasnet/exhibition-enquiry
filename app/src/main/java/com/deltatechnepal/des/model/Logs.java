package com.deltatechnepal.des.model;


public class Logs {

    private String customerName;
    private String customerEmail;
    private String companyName;
    private String companyMessage;


    public Logs() {

    }


    public Logs(String customerName, String customerEmail, String companyName, String companyMessage) {
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.companyName = companyName;
        this.companyMessage = companyMessage;

    }


    public String getCustomerEmail() {
        return customerEmail;
    }



    public String getCompanyMessage() {
        return companyMessage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCustomerName() {
        return customerName;
    }
}


