package com.deltatechnepal.des.helper;



import android.content.Context;
import android.content.SharedPreferences;

import com.deltatechnepal.des.model.Company;
import com.google.firebase.auth.FirebaseAuth;


public class SharedPreManager {

    //the constants
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_COMPANY_NAME = "keycompanyname";

    private static  SharedPreManager mInstance;
    private  Context mCtx;
    private FirebaseAuth mAuth;
    private SharedPreManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPreManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreManager(context);
        }
        return mInstance;
    }

    //method to let the user login
    //this method will store the user data in shared preferences
    public void userLogin(Company company) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_COMPANY_NAME, company.getcName());
        editor.apply();
    }
    public void customMessage(String value) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("customMessage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("message",value);
        editor.apply();
    }

    public String getcustomMessage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("customMessage", Context.MODE_PRIVATE);
        return sharedPreferences.getString("message","");
    }


    public void sortItemValue(int value) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("sortBy",value);
        editor.apply();
    }

    public void sortOrder(boolean value) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("sortOrder",value);
        editor.apply();
    }


    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_COMPANY_NAME, null) != null;
    }


    public int getSortValue() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("sortBy", 3);
    }


    public boolean isSortReversed() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("sortOrder",true);
    }


    //this method will give the logged in user
    public String getCompanyName() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_COMPANY_NAME, "");
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();


    }
}