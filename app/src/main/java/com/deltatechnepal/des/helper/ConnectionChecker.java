package com.deltatechnepal.des.helper;

import android.content.Context;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionChecker {

    public Context mCtx;
    private static  ConnectionChecker mInstance;

    public ConnectionChecker(Context context) {
        mCtx = context;
    }
    public static synchronized ConnectionChecker getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ConnectionChecker(context);
        }
        return mInstance;
    }


    public boolean Check() {
        ConnectivityManager cm =
                (ConnectivityManager) mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

}
