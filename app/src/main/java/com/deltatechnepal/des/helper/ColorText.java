package com.deltatechnepal.des.helper;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;

public class ColorText {

    public   Context mCtx;
    private static ColorText mInstance;

    public ColorText(Context context) {
        mCtx = context;
    }
    public static synchronized ColorText getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ColorText(context);
        }
        return mInstance;
    }


    public void multiColor(EditText v,String hint) {

        EditText tv =v;
        Spannable wordtoSpan = new SpannableString(hint);

        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), hint.length()-1, hint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv.setHint(wordtoSpan);
    }

}
