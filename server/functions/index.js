

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const postmarkTransport = require('nodemailer-postmark-transport');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

exports.sendEmail = functions.database.ref('/logs/{logId}').onWrite( event => {
 
   const snapshot = event.after.val();
   console.log(snapshot.customerEmail);


  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'info.exhibitionenquiry@gmail.com',
      pass: 'alpharomeo42'
    }
  });

   console.log(snapshot.companyEmail);

  	var mailOptions = {
    from: (snapshot.companyName).charAt(0).toUpperCase()+(snapshot.companyName).slice(1)+'<'+snapshot.companyEmail+'>',
    to: snapshot.customerEmail,
    subject: "Thank you for visiting our stall at CAN info-tech 2019",
    text:"Dear "+snapshot.customerName+",\n"+snapshot.companyMessage
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });

   return true;
  

});

exports.sendRecovery = functions.database.ref('/companies/{pushId}').onWrite( event => {
 
   const snapshot = event.after.val();
   if(snapshot.cForgot){
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'info.exhibitionenquiry@gmail.com',
      pass: 'alpharomeo42'
    }
  });

   console.log(snapshot.companyEmail);

  	var mailOptions = {
    from: "ExhibitionEnquiry App"+'<'+snapshot.companyEmail+'>',
    to: snapshot.cEmail,
    subject: "Recovery Code",
    text:"Please Enter this code in app for verification : "+snapshot.cRecovery
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}
   return true;
  

});
